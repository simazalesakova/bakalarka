% !TEX root = ../main.tex
\chapter{Gama-spektrometria a~detektory v~gama-spektrometrii}
Gama-spektrometria je~metóda používaná na~analýzu spektra vzoriek rôznych materiálov, pri ktorej dochádza k~identifikácii a~kvantifikácii rádioaktívnych izotopov vo~vzorke na~základe merania gama kvánt uvoľnených pri rozpade jadier. Štúdiom týchto kvánt, teda gama žiarenia, je~možné potom identifikovať o~aký rádioaktívny prvok sa~jedná. V~gama-spektrometrii sa~meria intenzita a~vlnová dĺžka žiarenia~\cite{spectrometry}.

\section{Žiarenie}
Žiarenie vznikajúce pri rozpade rádionuklidov sa~delí na~ionizujúce a~neionizujúce. Ionizujúce žiarenie je~charakteristické vysokou energiou kvánt, ktoré sú schopné vyrážať elektróny z~atómového obalu, čím danú látku ionizujú. Aby k~takejto ionizácii došlo, je~potrebné elektrónu v~atómovom obale predať vyššiu energiu než je~jeho väzbová energia. 
Ionizujúce žiarenie je~možné rozdeliť na~priamo ionizujúce a~nepriamo ionizujúce. Medzi priamo ionizujúce žiarenie patrí žiarenie, ktoré priamo vyráža elektróny z~atómového obalu prostredníctvom Coulombovských síl. Patria sem nabité častice, teda napr. protóny, elektróny, pozitróny a~alfa častice. Pri nepriamo ionizujúcom žiarení ide o~kvantá, ktoré nie sú nositeľmi elektrického náboja a~svoju kinetickú energiu najprv predávajú nabitým časticiam, ktoré potom spôsobia v~látke ionizáciu. Sem sa~zaraďujú neutróny, gama žiarenie a~röntgenové žiarenie~\cite{astronuklfyzika}.

\section{Interakcia gama žiarenia s~látkou}
Gama žiarenie je~fotónové žiarenie s~čiarovým spektrom, ktoré je~vysielané jadrami atómov pri rádioaktívnych premenách, a~ktoré sa~zaraďuje medzi nepriamo ionizujúce žiarenie kvôli absencii elektrického náboja. S~látkou interaguje s~určitou pravdepodobnosťou a~pri interakciách môže dôjsť k~predaniu významnej časti energie. 
Pri prechode látkou môže gama žiarenie interagovať tromi hlavnými spôsobmi, a~to fotoefektom, Comptonovým rozptylom a~tvorbou elektrón-pozitrónových párov. Tieto procesy vedú k~čiastočnému alebo úplnému predaniu energie gama fotónu elektrónu, následkom čoho je~rozptyl fotónu pod určitým uhlom alebo jeho úplný zánik~\cite{knoll, fbmi}.

Pravdepodobnosť výskytu jedného z~týchto troch procesov závisí na~energii fotónu a~protónovom čísle materiálu, s~ktorým fotón interaguje. Túto pravdepodobnosť interakcie s~atómom charakterizuje účinný prierez $\sigma$. Na~obr.~\ref{pravdepodobnost} je~znázornená závislosť výskytu interakcie na~energii žiarenia a~protónovom čísle materiálu. Pre nízke hodnoty energií (jednotky až desiatky keV) a~vysoké hodnoty protónového čísla bude dominovať fotoefekt. Pre vysoké energie (rádovo MeV) a~vysoké protónové čísla bude prevládať tvorba elektrón-pozitrónových párov, a~nakoniec, pre stredné hodnoty energií a~nízke protónové čísla bude prevládať Comptonov rozptyl~\cite{knoll}.

{
%\centering
\begin{figure}[H]
\begin{center}
  \includegraphics[width=1.0\textwidth]{../obrazky_bakalarka/pravdepodobnost.jpg}
  \end{center}
  \caption[Pravdepodobnosť výskytu jednotlivých interakcií gama žiarenia s~látkou]{Pravdepodobnosť výskytu jednotlivých interakcií gama žiarenia s~látkou~\cite{knoll}}
  \label{pravdepodobnost}
\end{figure}
}

\subsection{Fotoefekt}
Fotoefekt je~jav, pri ktorom nalietavajúci fotón interaguje s~elektrónom viazaným v~elektrónovom obale, predá mu~celú svoju energiu a~zanikne. K~fotoefektu dochádza len pri interakcii fotónu s~viazaným elektrónom, nie s~voľným elektrónom. Ak~je energia zaniknutého fotónu vyššia než väzbová energia elektrónu v~obale, tak dôjde k~uvoľneniu elektrónu z~obalu atómu. Energia uvoľneného elektrónu bude 
\begin{equation}
    E_\mathrm{e^-}= E_{\gamma} - E_\mathrm{v},
\end{equation}
kde $E_{\mathrm{e}^-}$ je~kinetická energia uvoľneného elektrónu, $E_{\gamma}$ je~energia fotónu a~$E_\mathrm{v}$ je~väzbová energia elektrónu v~atóme. 
Na prázdne miesto v~elektrónovom obale po~uvoľnenom elektróne preskočí elektrón z~vyššej vrstvy a~rozdiel energií medzi dvomi vrstvami sa~vyžiari vo~forme charakteristického röntgenového žiarenia alebo môže dôjsť k~predaniu energie elektrónu z~vyššej vrstvy, ktorý sa~potom uvoľní. Takéto elektróny sa~nazývajú Augerove elektróny. 
Fotoefekt je~dominantnou interakciou fotónov s~nízkymi energiami s~látkami o~vysokých protónových číslach. Neexistuje analytické vyjadrenie pravdepodobnosti fotoefektu platné pre všetky energie a~protónové čísla, existuje však približná aproximácia tejto pravdepodobnosti, a~to
\begin{equation}
    \sigma_\mathrm{f} \cong \mathrm{konšt} \times \frac{Z^n}{E_\gamma ^{3,5}},
\end{equation}
kde $\sigma_\mathrm{f}$ je~pravdepodobnosť, že dôjde k~fotoefektu, $Z$ je~protónové číslo, $E_\gamma$ je~energia fotónu a~exponent $n$ nadobúda hodnôt v~rozmedzí čísel 4 až 5. 
Fotoefekt stojí za~tvorbou píkov úplného pohltenia, FEP (\textit{full energy peak}), a~práve preto je~najviac využívaným javom v~gama-spektrometrii~\cite{astronuklfyzika, knoll}. Schéma fotoefektu je zobrazená na obr.~\ref{fotoefekt}.

{
%\centering
\begin{figure}[H]
\begin{center}
  \includegraphics[width=1.0\textwidth]{../obrazky_bakalarka/fotoefekt1.png}
  \end{center}
  \caption{Schéma fotoefektu}
 \label{fotoefekt}
\end{figure}
}

\subsection{Comptonov rozptyl}
Comptonov rozptyl je~najdominantnejším procesom interakcie gama žiarenia s~látkou. Prevláda u~gama žiarenia so~strednými energiami a~materiálov s~nízkym protónovým číslom. Gama fotón sa~zrazí s~voľným alebo slabo viazaným elektrónom, predá mu~časť svojej energie a~dôjde k~pružnému rozptylu fotónu pod uhlom $\vartheta$ vzhľadom na~jeho pôvodný smer. Odrazený fotón pokračuje teda v~pozmenenom smere a~s nižšou energiou.
Energia rozptýleného fotónu je~závislá na~uhle rozptylu $\vartheta$ a~je definovaná nasledovne:
\begin{equation}
    E_{\gamma'} = \frac{E_0^{\gamma}}{1+(\frac{E_0^{\gamma}}{E_0^e})(1-\cos \vartheta)},
\end{equation}
kde $E_{\gamma'}$ je~energia rozptýleného gama fotónu, $E_0^{\gamma}$ je~kinetická energia dopadajúceho gama fotónu pred zrážkou, $E_0^e$ je~pokojová energia elektrónu a~$\vartheta$ je~uhol rozptylu v~rozmedzí 0 až $\pi$. Fotón stratí tým viac energie, čím väčší je~uhol $\vartheta$, a~teda najviac energie stratí pri spätnom rozptyle, kedy $\vartheta=\pi$. Spätný rozptyl predstavuje interakciu, kedy je~odovzdávané najväčšie možné množstvo energie. Pri malých uhloch nedochádza k~takmer žiadnemu odovzdávaniu energie fotónom elektrónu~\cite{astronuklfyzika,knoll,mragheb}.

Pravdepodobnosť, že nastane Comptonov rozptyl, je~závislá na~protónovom čísle atómu \textit{Z}, na~ktorom k~rozptylu dochádza, a~na energii fotónu \textit{E}~\cite{gammaexplorer}:
\begin{equation}
    \sigma _\mathrm{c} \approx \mathrm{konšt} \cdot \frac{Z}{E}. 
\end{equation}
%

Comptonov jav v~spektre gama žiarenia spôsobuje Comptonovu hranu a~Comptonovo kontinuum. Schematické znázornenie Comptonovho rozptylu je na obr.~\ref{compton}.

{
%\centering
\begin{figure}[H]
\begin{center}
  \includegraphics[width=1.0\textwidth]{../obrazky_bakalarka/compton1.png}
  \end{center}
  \caption{Schéma Comptonovho rozptylu}
 \label{compton}
\end{figure}
}

\subsection{Tvorba elektrón-pozitrónových párov}
Tvorba elektrón-pozitrónových párov nastáva, keď je~energia fotónu väčšia ako súčet pokojových energií elektrónu a~pozitrónu. Elektrón a~pozitrón sú častice s~rovnakými pokojovými energiami a~hmotnosťami, a~opačnými nábojmi. Ich pokojová energia je 1,022~MeV. Preto je~tvorba párov dominantná pre vysoké energie gama fotónov. Interakcia musí prebiehať v~blízkosti jadra, ktoré môže byť nositeľom hybnosti a~energie. Pri energii fotónu väčšej ako 1,02~MeV je~prebytočná energia použitá vo~forme kinetickej energie elektrón-pozitrónového páru. Následne, po~tvorbe páru, pozitrón po~spomalení v~absorpčnom materiáli anihiluje s~elektrónom, za~vzniku dvoch anihilačných fotónov, ktoré predstavujú sekundárne produkty interakcie. Schematické znázornenie tvorby elektrón-pozitrónových párov je na obr.~\ref{pary}. Produkcia elektrón-pozitrónových párov je~zodpovedná za~vznik píkov SEP (\textit{single escape peak}) a~DEP (\textit{double escape peak}). Pík SEP vzniká, keď po~anihilácii elektrónu a~pozitrónu unikne jeden zo~sekundárnych fotónov z~detektora, a~má energiu o~hodnote $E_\gamma = \mathrm{FEP}-0,511~\mathrm{MeV}$. Pík DEP vzniká pri úniku oboch anihilačných fotónov a~má energiu $E_\gamma = \mathrm{FEP}-1,022~\mathrm{MeV}$. 
Pravdepodobnosť procesu vzniku párov závisí od~druhej mocniny protónového čísla~\cite{knoll, mragheb}: 
\begin{equation}
    \sigma_p \approx \mathrm{konšt} \cdot Z^2.
\end{equation}

{
%\centering
\begin{figure}[h]
\begin{center}
  \includegraphics[width=1.0\textwidth]{../obrazky_bakalarka/tvorbaparovnew.png}
  \end{center}
  \caption{Schéma tvorby elektrón-pozitrónových párov}
 \label{pary}
\end{figure}
}
\subsection{Ďalšie procesy v~spektre gama žiarenia}
Okrem už spomínaných troch procesov, fotoefekt, Comptonov rozptyl, tvorba elektrón-pozitrónových párov, sa~v spektre gama žiarenia prejavujú aj~iné javy. Jedným z~týchto efektov môže byť vznik anihilačného píku, ktorý má energiu 511~keV a~súvisí s~produkciou elektrón-pozitrónového páru v~tieniacom materiáli detektora mimo kryštál. Okrem elektrón-pozitrónového páru k~vzniku píku s~energiou 511~keV prispieva aj rozpad $\beta^+$ žiaričov. V~spektrometrii sa~objavuje aj~pík spätného odrazu, ktorý vzniká pri rozptýlení fotónov v~materiáli obklopujúcom detektor, odkiaľ sa~odrazia späť smerom ku~kryštálu, a~následne sa~detekujú. Na začiatku spektra je~možné vidieť impulzy s~nízkymi amplitúdami, ktoré predstavujú nežiaduci jav a~nazývajú sa~šumom. Šum predstavuje určitú prekážku pri detekcii, dá sa~však zredukovať chladením, napr. pomocou kvapalného dusíka. Ďalším javom je~sumačný pík, pri ktorom sa~sčítajú energie dvoch píkov a~vznikne nový pík, ktorého amplitúda je~rovná súčtu energií jednotlivých píkov~\cite{astronuklfyzika, gammaexplorer}. Spektrum gama žiarenia je zobrazené na obr.~\ref{spektrum}.

{
%\centering
\begin{figure}[h]
\begin{center}
  \includegraphics[width=1.0\textwidth]{../obrazky_bakalarka/spektrum1.png}
  \end{center}
  \caption[Spektrum gama žiarenia]{Spektrum gama žiarenia~\cite{spektrumobr}} 
 \label{spektrum}
\end{figure}
} 

%************OBRAZOK SPEKTRA GAMA ZIARENIA UPRAVIT*****************
\section{Detektory používané v~gama-spektrometrii}
V gama-spektrometrii sa~požívajú dva hlavné typy detektorov, a~to polovodičové a~scintilačné detektory. Scintilačné detektory sa delia na~organické a~anorganické.
\subsection{Polovodičové detektory}
Polovodičové detektory pozostávajú zo~systému, ktorý obsahuje samotný detektor, zosilňovač, analógovo-digitálny konvertor a~mnohokanálový analyzátor.
Detektor je~tvorený elektródami pripojenými k~elektrickému obvodu s~vysokým napätím~(1 až 2~kV) a~umiestnenými v~polovodičovom materiáli, čo predstavuje diódu zapojenú v~závernom smere, kedy v~pokojovom stave bez prítomnosti ionizujúceho žiarenia ňou neprechádza prúd.
Po vniknutí ionizujúceho žiarenia do~detektora dôjde v~polovodiči k~preskočeniu elektrónov do~vodivého pásma a~tým k~vzniku elektrón-dierových párov. Následne dochádza k~pohybu elektrónov smerom k~anóde a~dier ku~katóde, čo spôsobí prechod krátkeho prúdového impulzu elektrickým obvodom.
Na veľkom ohmickom odpore dôjde k~vzniku napäťového impulzu, ktorý sa~cez kondenzátor vedie do~predzosilňovača, z~ktorého sa~čiastočne zosilnený signál vedie do~zosilňovača a~zosilní sa~na potrebnú úroveň.
Zosilnené impulzy putujú ďalej cez analógovo-digitálny konvertor do~mnohokanálového analyzátora, kde prebieha amplitúdová analýza výstupných impulzov. 

Najčastejšie používanými polovodičovými detektormi sú kremíkové a~germániové detektory z~monokryštálov germánia. Atómové číslo germánia je~vyššie ako atómové číslo kremíku a~zároveň má vyšší absorpčný koeficient pre gama žiarenie, preto je~pre jeho detekciu účinnejšie. Germániové detektory sú vhodné pre detekciu energií rádovo niekoľko MeV a~kremíkové detektory sú vhodné pre nízke energie, rádovo v~jednotkách keV.
Pod samotným telesom detektora sa~zvyčajne nachádza Dewarova nádoba naplnená kvapalným dusíkom, prostredníctvom ktorého sa~detektory chladia. Toto chladenie na~teplotu kvapalného dusíka má prínos v~znížení elektronického šumu a~záverného prúdu~\cite{astronuklfyzika}. Na obr.~\ref{hpge} sa nachádza fotografia pracoviska s~HPGe detektorom na KJR FJFI ČVUT.
%PRIDAT NEJAKY OBRAZOK ALEBO SCHEMU detektora
{
%\centering
\begin{figure}[H]
\begin{center}
  \includegraphics[width=1.0\textwidth, angle=360]{../obrazky_bakalarka/hpge_skola.jpg}
  \end{center}
  \caption{Pracovisko s~HPGe detektorom na~KJR v~Prahe} 
 \label{hpge}
\end{figure}
} 

\subsection{Scintilačné detektory}
Princíp fungovania scintilačných detektorov je~založený na~jave, ktorý sa~nazýva rádioluminiscencia. Je~to vlastnosť látok, kedy sú v~materiáli produkované scintilácie (svetelné záblesky) pri dopade ionizujúceho žiarenia. Materiály s~touto schopnosťou sa~nazývajú scintilátory. 
Najčastejšie používaným scintilátorom je~kryštál NaI(Tl), teda jodid sodný aktivovaný thaliom. Je~vhodný hlavne pre detekciu nižších a~stredných energií gama žiarenia a~patrí medzi anorganické scintilátory. Medzi organické scintilátory sa~zaraďuje napr. antracén, ktorý sa~využíva pri porovnávaní vlastností organických scintilačných materiálov, ďalej naftalén, stilbén. Detekčná účinnosť organických scintilátorov je~pre gama žiarenie nízka kvôli ich nízkej hustote pre detekciu tohto druhu žiarenia. Používajú sa~však na~detekciu ostatných druhov žiarenia, teda alfa, beta, protónov či rýchlych neutrónov.

Celý systém detektora pozostáva zo~scintilačného kryštálu, za~ktorým nasleduje fotonásobič pozostávajúci z~fotokatódy, ktorá pri dopade ionizujúceho žiarenia pomocou fotoefektu konvertuje svetelné fotóny na~elektróny. Tie sa~potom násobia v~systéme dynód, na~ktoré je~privádzané kladné napätie, ktorého hodnota sa~postupne stupňuje a~tým sa~zvyšuje počet vyrazených sekundárnych elektrónov z~povrchu dynódy. Vďaka opakovanému násobeniu sa~pôvodný počet elektrónov z~fotokatódy znásobí a~na anódu dopadajú v~dostatočnom počte na~vyvolanie dobre merateľného elektrického impulzu. % Vo~fotonásobiči sú teda registrované svetelné záblesky premieňané na~elektrický signál.
Ďalej nasleduje zosilňovač signálov a~elektronické spracovanie signálov v~podobe analógovo-digitálneho konvertora a~mnohokanálového analyzátora alebo jednokanálového amplitúdového analyzátora a~čítača impulzov~\cite{astronuklfyzika}.
%OBRAZOK ZAPOJENIA A~SCHEMA astronuklfyzika

